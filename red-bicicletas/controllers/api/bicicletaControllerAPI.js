var Bicicleta = require('../../models/bicicleta');

exports.bicicleta_list = function(req,res){
    res.status(200).json({
        bicicletas: Bicicleta.allBicis
    });
}

exports.bicicleta_create = function(req,res){
    var bici = new Bicicleta(req.body.code, req.body.color, req.body.modelo);
    bici.ubicacion = [req.body.lat, req.body.lng];

    Bicicleta.add(bici);

    res.status(200).json({
        bicicletas: bici
    });
}

exports.bicicleta_delete = function(req,res){
    Bicicleta.removeById(req.body.id);
    res.status(204).send();
    
}

exports.bicicleta_update = function(req,res){
    var bici = Bicicleta.findByCode(req.body.code);

    var biciUpdate = { 
        code: bici.code,
        color: req.body.color,
        modelo: req.body.modelo,
    };

    biciUpdate.ubicacion = [req.body.lat, req.body.lng];

    Bicicleta.updateBici(biciUpdate,(err,res) =>{
        if (err) console.log(err);
        res.status(200).json(res);
    });

}