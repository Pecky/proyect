var Usuario = require('../models/usuario');
var Token = require('../models/token');
const usuarios = require('./usuarios');

module.exports={
    confirmationGet: function(req,res,next){
        Token.findOne({token: req.params.token}, function(err, token){
            if(!token) return res.status(400).send({typwe:'not-Verified',msg:'No encontramos un usuario con este token. Quiza haya expirado y debas solicitar uno nuevo'});
            Usuario.findById(token_userId, function(err,usuario){
                if(!usuario) return res.status(400).send({msg: 'No encontramos un usuario con este token'});
                if(usuario.verificado) return res.redirect('/usuarios');
                usuarios.verificado = true;
                usuario.save(function(err){
                    if(err){return res.status(500).send({msg: err.message}); }
                    res.redirect('/');
                });
            });
        });
    },
}