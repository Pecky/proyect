var mongoose = require('mongoose');
var Bicicleta = require('../../models/bicicleta');
var request = require('request');
var server = require('../../bin/www');
const { application } = require('express');
const { create } = require('../../models/bicicleta');

var base_url = "http://localhost:5000/api/bicicletas";

//beforeEach(() => {Bicicleta.allBicis = []; });
describe ('Bicicleta Api', () => {
    beforeEach(function(done){
        var mongoDB = 'mongodb://localhost/testdb';
        mongoose.connect(mongoDB, { useNewUrlParser: true});

        const db = mongoose.conection;
        db.on('error', console.error.bind(console, 'connection error'));
        db.once('open', function(){
            console.log('we are connected to test database');
            done();
        });
    });

    afterEach(function(done){
        Bicicleta.deleteMany({},function(err,success){
            if(err) console.log(err);
            done();
        });
    });


    describe('GET BICICLETAS /', () => {
        it('Status 200', (done) =>{
            request.get(base_url,function(error,response,body){
                var result = JSON.parse(body);
                expect(response.statusCode).toBe(200);
                expect(result.Bicicletas.length).toBe(0);
                done();
            });
            
        });
    });

    describe('POST BICICLETAS /create', () => {
        it('STATUS 200', (done) => {
            var headers = {'content-type' : 'application/json'};
            var aBici = '{"id": 10, "color": "rojo", "modelo": "urbana", "lat": 3, "lng": -76}';
            request.post({
               headers: headers,
               url:     base_url + '/create',
               body:    aBici 
            },  function(error,response,body) {
                expect(response.statusCode).toBe(200);
                var bici = JSON.parse(body).bicicleta;
                console.log(bici);
                expect(bici.color).toBe("rojo");
                expect(bici.ubicacion[0]).toBe(3);
                expect(bici.ubicacion[1]).toBe(-76);
                done();
            });
        });
    });

    

    /* describe('UPDATE BICICLETAS/', () => {
        it('Status 200', (done) =>{
            var a = new Bicicleta(5,'blanca','urbana',[3.605397,-76.7657528]);
            Bicicleta.add(a);

            var headers = {'content-type' : 'application/json'};
            var aBici = '{"id": 5, "color": "rojo", "modelo": "urbana", "lat": 3, "lng": -76}';
            request.post({
               headers: headers,
               url:     'http://localhost:3000/api/bicicletas/update',
               body:    aBici 
            },  function(error,response,body) {
                expect(response.statusCode).toBe(200);
                expect(Bicicleta.findById(5).color).toBe("rojo");
                expect(Bicicleta.findById(5).modelo).toBe("urbana");

                done();
            });
        });
    });

    describe('DELETE BICICLETAS/', () => {
        it('Status 204', () =>{
            expect(Bicicleta.allBicis.length).toBe(3);

            //var a = new Bicicleta(3,'blanca','urbana',[3.605397,-76.7657528]);
            //Bicicleta.add(a);
            Bicicleta.removeById(2);
            Bicicleta.removeById(5);
            Bicicleta.removeById(10);

            request.get('http://localhost:3000/api/bicicletas', function(error,response,body){
                expect(response.statusCode).toBe(204);
            });
        });
    }); */


});